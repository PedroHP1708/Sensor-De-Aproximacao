# Sensor De Aproximação

A ideia por trás desse projeto foi desenvolver um sensor de aproximação utilizando Arduino, que utiliza um sensor de distância ultrassônico e um buzzer, para
indicar a proximidade de objetos com base na frequência sonora.

# Componentes

1. Arduino UNO
2. Protoboard
3. Sensor de Distância Ultrassônico
4. Sensor Piezo Buzzer
5. Display LCD 16x2 (I2C)

# Grupo

* Glauco Fleury Correa de Moraes      15456302
* João Ricardo de Almeida Lustosa     15463697
* Leonardo Massuhiro Sato             15469108
* Pedro Henrique Perez Dias           15484075
* Pedro Lunkes Villela                15463697

# Imagem

![plot](foto_projeto.jpeg)

# Video da demonstração

https://www.youtube.com/watch?v=zY_TXdcTC_g