// Constantes utlizadas
#include <LiquidCrystal_I2C.h>

#define trPin 3
#define ecPin 2

#define botao 6

#define buzzerP 11

LiquidCrystal_I2C lcd (0x27, 16, 2);

class Buzzer
{
    public:
           Buzzer(int pin);
           void tocar(int note, int duration);
    private:
            int buzzerPin;
};

Buzzer buzzerI(buzzerP);

//Inicialização dos componentes
void setup()
{
  lcd.init();
  lcd.begin(16, 2);
  lcd.setBacklight(HIGH);
  delay(100); // para garantir a inicialização do modulo
  lcd.clear();  // limpa o display
  lcd.setCursor(0,0); 
  pinMode(trPin, OUTPUT);
  pinMode(ecPin, INPUT);

  
  pinMode(buzzerP, OUTPUT);
}

void loop()
{
  float duracao;
  float cm;
  float pol;
  
  digitalWrite(trPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trPin, LOW);
  // Espera a resposta , sendo esta uma entrada
  duracao = pulseIn(ecPin, HIGH);

  
  cm = (duracao /29 /2);  // Faz conversão de tempo para distância
  
  lcd.clear();
  lcd.setCursor(0,0);

  lcd.print("cm: ");
  lcd.setCursor(0, 1);
  lcd.print(cm, 1);       // Arredonda o valor de 'cm' para uma casa decimal
  
  
  if(cm > 30)
  {
    delay(1000);
  }
  
  if(cm <= 30 && cm > 20)     // Lento
  {
    buzzerI.tocar(100, 1000);
  }
    
  if(cm <= 20 && cm > 5)      // Médio
  {
    buzzerI.tocar(100, 500);
  }
    
  if(cm <= 5)                 // Rápido
  {
    buzzerI.tocar(100, 100);
  }
  
}

/*********************Classe*************************/

Buzzer::Buzzer(int pin)
{
  buzzerPin = pin;
}

/*********************Método*************************/
void Buzzer::tocar(int note, int duration)
{
      tone(buzzerPin, note /4);
      delay(duration);   
      noTone(buzzerPin);
      delay(300);
}
